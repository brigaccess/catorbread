import * as React from 'react';

import { getRandomBread, getRandomCat } from '../../DefinitelyNotAServer';
import GameScreen from '../gamescreen/GameScreen';

import { Participant, ParticipantType } from '../../Participant';

interface State {
    active: boolean;
    bread: boolean;
    locked: boolean;
    intervalId?: any;
    lastClick: number;
    participants: Participant[];
    score: number;
    timeLeft: number;
}

const difficultyParticipants : Participant[] = [
    {
        photoUrl: 'https://www.intel.com/content/dam/products/hero/foreground/processor-badge-xeon-1x1.png.rendition.intel.web.293.293.png',
        type: ParticipantType.BREAD
    }, {
        photoUrl: 'https://www.intel.com/content/dam/products/hero/foreground/processor-badge-celeron-1x1.png.rendition.intel.web.293.293.png',
        type: ParticipantType.BREAD
    },
]

class GameController extends React.Component<object, State> {
    constructor(props: object) {
        super(props);
        this.state = {
            active: false,
            bread: false,
            lastClick: 0,
            locked: false,
            participants: difficultyParticipants,
            score: 0,
            timeLeft: 0,
        }
    }

    public componentDidMount = () => {
        window.addEventListener('keydown', this.keydownListener);
    }

    public componentWillUnmount = () => {
        window.removeEventListener('keydown', this.keydownListener);
    }

    public keydownListener = (event: KeyboardEvent) => {
        if (event.keyCode === 37) {
            this.onClick(1);
        } else if (event.keyCode === 39) {
            this.onClick(2);
        }
    }

    public componentDidUpdate = () => {
        if (this.state.active && this.state.timeLeft <= 0) {
            this.endGame();
        }
    }

    public addPoint = (amount: number) => {
        this.setState((state: State) => ({
            score: state.score + amount
        }));
    }

    public onClick = (index: number) => {
        if (this.state.locked) {
            return;
        }
        // If the game is already started, check if player is correct
        if (this.state.active) {
            const diff = new Date().getTime() - this.state.lastClick;
            if (diff > 300) {
                const isBread = this.state.participants[index - 1].type === ParticipantType.BREAD
                if (this.state.bread && isBread || !this.state.bread && !isBread) {
                    this.addPoint(1);
                } else {
                    this.addPoint(-1);
                }
                this.generateTask();
                this.setState({
                    lastClick: new Date().getTime()
                })
            }
        } else {
            // Game is not started => start the game
            this.startGame(15 * index);
        }
    }

    public resetLock = () => {
        this.setState({
            locked: false
        });
    }

    public render() {
        let content = "How fast are you?";
        if (this.state.active) {
            content = "Find a " + ((this.state.bread) ? "BREAD" : "CAT");
        }
        return (
            <GameScreen leftParticipant={this.state.participants[0]} rightParticipant={this.state.participants[1]} locked={this.state.locked} onClick={this.onClick}>
                <div>
                    {!this.state.locked && <div>{content}</div>}
                    {(this.state.active || this.state.locked) && <div>Score: {this.state.score}</div>}
                    <div>{this.state.active && "Time: " + this.state.timeLeft}</div>
                </div>
                <div className="GameController-restart">
                    {this.state.locked && <button onClick={this.resetLock}>🔁</button>}
                </div>
            </GameScreen>
        )
    }

    private subtractTime = () => {
        this.setState((state: State) => ({
            timeLeft: state.timeLeft - 1
        }));
    }

    private startGame = (time: number) => {
        this.generateTask();
        this.setState((state: State) => ({
            active: true,
            intervalId: setInterval(this.subtractTime, 1000),
            lastClick: 0,
            score: 0,
            timeLeft: time,
        }));
    }

    private endGame() {
        clearInterval(this.state.intervalId);
        this.setState({
            active: false,
            locked: true,
            participants: difficultyParticipants,
            timeLeft: 0
        });
    }

    private generateTask = () => {
        const breadLink = getRandomBread();
        const catLink = getRandomCat();

        const bread : Participant = {
            photoUrl: breadLink,
            type: ParticipantType.BREAD
        }

        const cat : Participant = {
            photoUrl: catLink,
            type: ParticipantType.CAT
        }

        const isBread = (Math.floor(Math.random() * 2)) === 0;
        const breadOffset = (Math.floor(Math.random() * 2));
        const newParticipants = []
        newParticipants[breadOffset % 2] = cat;
        newParticipants[(breadOffset + 1) % 2] = bread;

        this.setState({
            bread: isBread,
            participants: newParticipants
        });
    }
}

export default GameController