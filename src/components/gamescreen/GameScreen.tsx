import * as React from 'react';

import './GameScreen.css';

import { Participant } from '../../Participant';
import ParticipantVotePanel from '../participantvotepanel/ParticipantVotePanel';

export interface Props {
    leftParticipant: Participant;
    locked: boolean;
    onClick: (index: number) => void;
    rightParticipant: Participant;
}

class GameScreen extends React.Component<Props, object> {
    public onClickLeft = () => this.props.onClick(1);
    public onClickRight = () => this.props.onClick(2);

    public render() {
        const left = this.props.leftParticipant;
        const right = this.props.rightParticipant;

        return (
            <div className="GameScreen">
                <div className="GameScreen-header">
                    <h1>Cat vs. Bread</h1>
                    <h4>NO EAT TIHNK FIRTS!</h4>
                </div>

                <div className="GameScreen-content">
                    <div className="GameScreen-votepanel GameScreen-participant-left fadeIn">
                        <ParticipantVotePanel name={left.name || "THIS!"} photoUrl={left.photoUrl} onClick={this.onClickLeft} locked={this.props.locked}/>
                    </div>
                    <div className="GameScreen-middle fadeIn">
                        {this.props.children}
                    </div>
                    <div className="GameScreen-votepanel GameScreen-participant-right fadeIn">
                        <ParticipantVotePanel name={right.name || "THAT!"} photoUrl={right.photoUrl} onClick={this.onClickRight} locked={this.props.locked}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default GameScreen;