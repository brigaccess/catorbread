import * as React from 'react';

import ParticipantPhoto from '../participantphoto/ParticipantPhoto';
import './ParticipantVotePanel.css';

interface ParticipantProps {
    locked: boolean;
    name: string;
    photoUrl: string;
    onClick: React.MouseEventHandler;
}

// <button className="ParticipantVotePanel-voteBtn" disabled={locked} onClick={onClick}>{name}</button>

const ParticipantVotePanel: React.FunctionComponent<ParticipantProps> = (
    {locked, name, photoUrl, onClick}: ParticipantProps
) =>
<div className={"ParticipantVotePanel" + (locked && " ParticipantVotePanel-locked")} >
    <div className="ParticipantVotePanel-photo" onClick={onClick}>
        <ParticipantPhoto photoUrl={photoUrl} />
    </div>
</div>

export default ParticipantVotePanel;