import * as React from 'react';

import './ParticipantPhoto.css';

export interface ParticipantPhotoProps {
    photoUrl?: string;
}

interface State {
    hidden: boolean;
    hiding: boolean;
    loaded: boolean;
    newPhotoUrl?: string;
    photoUrl?: string;
    timeoutId?: any;
}

class ParticipantPhoto extends React.Component<ParticipantPhotoProps, State> {
    constructor(props: ParticipantPhotoProps) {
        super(props);

        this.state = {
            hidden: true,
            hiding: false,
            loaded: false,
            newPhotoUrl: this.props.photoUrl,
        }
    }

    public componentDidUpdate = (prevProps: ParticipantPhotoProps, prevState: State) => {
        // Hide photo if changed
        if (prevProps.photoUrl !== this.props.photoUrl) {
            this.setState((state: State) => {
                if (state.timeoutId) { clearTimeout(state.timeoutId); }
                return {
                    hiding: true,
                    loaded: false,
                    newPhotoUrl: this.props.photoUrl,
                    // onTransitionEnd doesn't work properly when changing pictures really fast
                    // Which is why there is a timeout that will unscrew the situation
                    timeoutId: setTimeout(
                        () => {this.setState({hidden:true, hiding:false})}, 300
                    )
                }});

            return;
        }

        if (this.state.hidden && this.state.loaded) {
            this.setState({
                hidden: false,
                photoUrl: this.state.newPhotoUrl
            })
        }
    }

    public render() {
        const photoUrl = this.state.photoUrl;
        const divStyle : React.CSSProperties = {
            opacity: this.state.hiding || this.state.hidden ? 0 : 1,
            transition: 'opacity 0.3s linear'
        };

        if (photoUrl) {
            divStyle.backgroundImage = 'url(' + photoUrl + ')';
        }

        return (
            <div className="ParticipantPhoto">
                <div className="ParticipantPhoto-photo" style={divStyle}>
                    <img src={this.state.newPhotoUrl} onLoad={this.onImageLoad} />
                </div>
            </div>
        );
    }

    private onImageLoad = () => {
        this.setState({
            loaded: true
        });
    }
}
export default ParticipantPhoto;