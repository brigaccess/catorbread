export enum ParticipantType {
    CAT, BREAD
}

export interface Participant {
    name?: string;
    type: ParticipantType;
    photoUrl: string;
}