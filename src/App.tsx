import * as React from 'react';
import './App.css';

import GameController from './components/gamecontroller/GameController'

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <GameController />
      </div>
    );
  }
}

export default App;
